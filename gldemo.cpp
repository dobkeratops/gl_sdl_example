//#include "shadertest.h"
#include <math.h>
#include <string.h>
#define PI M_PI

#include <vector>
#include <array>

using namespace std;
#define fbx_printf printf
//#define ASSERT(X)
//#include "texture.h"
int getTexture(const char*);

#define GL_GLEXT_PROTOTYPES
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include <GL/glext.h>
#include <GL/gl.h>
#include <GL/glu.h>

enum 
{	VertexAttrIndex_pos,VertexAttrIndex_color,VertexAttrIndex_norm,VertexAttrIndex_tex0,VertexAttrIndex_tex1,VertexAttrIndex_count
};

struct	TestVertex 
{
	array<float,3>	pos;
	array<float,4>	color;
	array<float,3>	norm;
	array<float,2>	tex0;
};


struct	Mesh // holds a gl mesh - vao describing layout & buffer, vbo,index buffer, primitive counts
{			// TODO - bounding box information, material organization,optional CPU copy,..
	typedef int IndexType;
	enum {IndexSize = sizeof(IndexType) };
	int	vertexSize;
	typedef	::TestVertex Vertex;
	GLuint vao;
	int	vbo;
	int	ibo;
	int	numVertices,numIndices;
};

int	g_Texture[5];
int	g_ShaderProgram,g_PixelShader,g_VertexShader; // TODO - propper manager for shaders, hash to find pairings..


inline auto SetMatrixProjectionFrustumDemo(float m[16], float left, float right, float bottom, float top, float fnear, float ffar)->void
{
    auto a=(right+left)/(right-left);
    auto b=(top+bottom)/(top-bottom);
    auto c=-(ffar+fnear)/(ffar-fnear);
    auto d=-(2*ffar*fnear/(ffar-fnear));
#define STORE4(D,I, X,Y,Z,W) (D)[0+I]=X; (D)[4+I]=Y; (D)[8+I]=Z; (D)[12+I]=W;
    STORE4(m,0, 2.f*fnear/(right-left), 0.f,0.f,0.f);
    STORE4(m,1, 0.f, 2.f*fnear/(top-bottom), 0.f,0.f);
    STORE4(m,2, a,b,c,-1.f);
    STORE4(m,3, 0.f,0.f,d,0.f);
#undef STORE4
}



int 
GFX_CreateAndCompileShader(int shaderType, const char* source) 
{
	int	shader = glCreateShader(shaderType);
	const char* pSrc[1]={source};
	int length[1] = {(int)strlen(source)};
	glShaderSource(shader, 1,pSrc,length);
	glCompileShader(shader);
	int	status;glGetShaderiv(shader,GL_COMPILE_STATUS,&status);
	if (status==GL_FALSE) 
	{
		char	log[512]; int len;
		glGetShaderInfoLog(shader, 512,&len, log);
		printf("\n%s\n", source, log);
		printf("compile shader[%d] failed: \n%s\n", shader, log);
		exit (0);
	}	
	else printf("create shader[%d] - compile suceeded\n",  shader);
	return	shader;
}


struct ShaderProgRet {
	int pixel_shader;
	int vertex_shader;
	int program;
};

ShaderProgRet	GFX_CreateShaderProgram(
			const char* pixelShaderSource,
			const char* vertexShaderSource,
			int* pixelShaderOut, 
			int* vertexShaderOut, 
			int* programOut)
{
	*vertexShaderOut = GFX_CreateAndCompileShader(GL_VERTEX_SHADER, vertexShaderSource);
	*pixelShaderOut = GFX_CreateAndCompileShader(GL_FRAGMENT_SHADER, pixelShaderSource);
	int	prog = glCreateProgram();
	// assign attribute names before linking

	glAttachShader(prog, *pixelShaderOut);
	glAttachShader(prog, *vertexShaderOut);
	

	printf("linking verteshader[%d], pixelshader[%d] to program[%d]\n", *vertexShaderOut, *pixelShaderOut, prog);
	glLinkProgram(prog);

	int err;
	glGetProgramiv(prog,GL_LINK_STATUS,&err);
	if (err==GL_INVALID_VALUE || err==GL_INVALID_OPERATION) {	
		char buffer[1024];int len;
		glGetProgramInfoLog(*programOut,1024,&len,buffer);
		printf("link program failed:");
		printf("%s", buffer);
	} else printf("link program status %d\n", err);
	*programOut = prog;
	return ShaderProgRet{*pixelShaderOut,*vertexShaderOut,prog};
}

//TODO: split into default uniforms, default vertex, default vertex-shader-out

#define PS_VS_INTERFACE0 \
"varying	vec4 v_pos;\n" \
"varying	vec4 v_color;\n" \
"varying	vec3 v_norm;\n" \
"varying	vec2 v_tex0;\n" \
"varying	vec3 v_tex1;\n" \
"varying	vec4 v_tangent;\n"\
"varying	vec4 v_binormal;\n"


#define PS_VERTEX_FORMAT0 \
"layout(location=0) in vec3 a_pos;\n" \
"layout(location=1) in vec4 a_color;\n" \
"layout(location=2) in vec3 a_norm;\n" \
"layout(location=3) in vec2 a_tex0;\n" \

#define SHADER_UNIFORMS \
"layout(location=0) uniform mat4 uMatProj;\n" \
"layout(location=4) uniform mat4 uMatModelView;\n" \
"layout(location=8) uniform vec4 uAmbient;\n" \
"layout(location=9) uniform vec4 uDiffuseDX;\n" \
"layout(location=10) uniform vec4 uDiffuseDY;\n" \
"layout(location=11) uniform vec4 uDiffuseDZ;\n" \
"layout(location=12) uniform vec4 uFogColor;\n" \
"layout(location=13) uniform vec4 uFogFalloff;\n" \
"layout(location=14) uniform vec4 uSpecularDir;\n" \
"layout(location=15) uniform float uSpecularPower;\n" \
"layout(location=16) uniform vec4 uSpecularColor;\n" \
"layout(location=17) uniform sampler2D uTex0;" \
"layout(location=18) uniform sampler2D uTex1;" \
"layout(location=19) uniform sampler2D uTex2;" \
"layout(location=20) uniform sampler2D uTex3;" \
"layout(location=21) uniform int uNumLights;" \
"layout(location=22) uniform vec4 uLightPos[4];\n" \
"layout(location=26) uniform vec4 uLightColor[4];\n" \


const char g_VS_Default[]=
"#version 330\n"
"#extension GL_ARB_explicit_uniform_location : require\n"
"#define varying out\n"
PS_VERTEX_FORMAT0
PS_VS_INTERFACE0
SHADER_UNIFORMS
"void main() {\n"
"	vec4 epos = uMatModelView * vec4(a_pos.xyz,1.0);\n"
"	vec3 enorm = (uMatModelView * vec4(a_norm.xyz,0.0)).xyz;\n"
"	vec4 spos=uMatProj * epos;\n"
"	gl_Position = spos;\n"
"	v_pos = epos;\n"
"	v_color = a_color;\n"
"	v_tex0 = a_tex0;\n"
"	v_tex1 = a_pos.xyz;\n"
"	v_norm = enorm;\n"
"}";

const char g_VS_PassThru[]=
"#version 330\n"
"#extension GL_ARB_explicit_uniform_location : require\n"
"#define varying out\n"
PS_VERTEX_FORMAT0
PS_VS_INTERFACE0
SHADER_UNIFORMS
"void main() {\n"
"	vec4 epos = uMatModelView * vec4(a_pos.xyz,1.0);\n"
"	vec3 enorm = (uMatModelView * vec4(a_norm.xyz,0.0)).xyz;\n"
"	vec4 spos=uMatProj * epos;\n"
"	gl_Position = vec4(a_pos.xyz,1.0);\n"
"	v_pos = epos;\n"
"	v_color = a_color;\n"
"	v_tex0 = a_tex0;\n"
"	v_tex1 = a_pos.xyz;\n"
"	v_norm = enorm;\n"
"}";

/*
cases:
VSO:
	static scene#define SH_LOG printf

	animation,3bone
PS:
	2textures
	3textures
 */

// 2 texture layers, blended using vertex alpha
const char g_PS_Alpha[]= 
"#version 330\n"
"#extension GL_ARB_explicit_uniform_location : require\n"
"#define varying in\n"
PS_VS_INTERFACE0
SHADER_UNIFORMS

"vec4 applyFog(vec3 pos, vec4 color){\n"
"	return mix(color,uFogColor,  clamp(-uFogFalloff.x-pos.z*uFogFalloff.y,0.0,1.0));\n"
"}\n"
"vec4 pointlight(vec3 pos, vec3 norm,vec3 lpos, vec4 color, float falloff) {\n"
"	vec3 dv=lpos-pos;\n"
"	float d2=sqrt(dot(dv,dv));\n"
"	float f=clamp( 1.0-(d2/falloff),0.0,1.0);\n"
"	vec3 lv=normalize(dv);\n"
"	return clamp(dot(lv,norm),0.0,1.0) * f*color;\n"
"}\n"
"void main() { \n"
"	float inva=(v_color.w),a=(1-v_color.w);\n"
"	vec4 tex0color=texture2D(uTex0, v_tex0);\n"
"	vec4 tex1color=texture2D(uTex1, v_tex0);\n"
// TODO - check this specular stuff, looks dodgy!
"	vec3 fromEye=normalize(v_pos.xyz);\n" // reflected vector from eye for specular higlight
"	vec3 refl = fromEye-v_norm*2.0*dot(fromEye,v_norm);\n"
"	float highlight=max(0.f,dot(refl,uSpecularDir.xyz));\n"
	"	highlight=(highlight*highlight);highlight=highlight*highlight;\n"
"	vec4 surfaceColor=mix(tex0color,tex1color,v_color.w);\n"
// TODO - specular is best controlled with a surface channel eg alpha. 
// we have aproximated it as the upper half of the color range
//"	vec4 surfaceSpec=clamp(4*(surfaceColor-vec4(0.5,0.5,0.5,0.0)), vec4(0.0,0.0,0.0,0.0),vec4(1.0,1.0,1.0,1.0));\n"
"   float surfaceSpec = clamp(2.0*sqrt(dot(surfaceColor.xyz,surfaceColor.xyz)/3)-0.5 ,0.0, 1.0);\n"
"	vec4 spec=highlight*uSpecularColor*surfaceSpec;\n"
"	vec4 diff=uAmbient+v_norm.x*uDiffuseDX+v_norm.y*uDiffuseDY+v_norm.z*uDiffuseDZ;\n"
"	float lx=0.5,ly=0.5;\n"
"	for (int i=0; i<uNumLights; i++) {     \n"
"		diff +=pointlight(v_pos.xyz, v_norm.xyz, uLightPos[i].xyz, uLightColor[i].xyzw, uLightPos[i].w);\n"
"	}\n"
"	gl_FragColor =applyFog(v_pos.xyz,surfaceColor*diff*vec4(v_color.xyz,0.0)*2.f+spec);\n"
"}";


void	CreateShaders()
{
	GFX_CreateShaderProgram(g_PS_Alpha, g_VS_Default,  &g_VertexShader,&g_PixelShader, &g_ShaderProgram);
}

struct	GridMesh : Mesh
{
	GridMesh(int	usize, int vsize);
};


/*
// number of shaders:-
lights 1-4	x
skinning 1,3 x
texturecoords x

pixel shaders:-
simple
2layer
3layer alpha blend
3layer sea-blend

GetShader(numLights, numTexcoords, numBones);
*/

#define SETARRAY2(dst, v0,v1) { dst[0]=v0; dst[1]=v1;}
#define SETARRAY3(dst, v0,v1,v2) { dst[0]=v0; dst[1]=v1; dst[2]=v2;}

void
FillTorusVertices(TestVertex* dv, int numU,int numV) 
{
	int	i,j;
	float fi=0.f, fj=0.f, dfi=1.f/(float)(numU-1),  dfj=1.f/(float)(numV-1);

	float rx=0.05f,ry=rx*0.33f;
	const float pi=3.14159265,pi2=pi*2.f;
	for (fj=0.f,j=0; j<numV; j++,fj+=dfj) 
	{
		for (fi=0.f,i=0; i<numU; i++,fi+=dfi, dv++) 
		{
			int dvi=i+j*numU;
			float cx = sin(2.f*pi*fi);
			float sx = cos(2.f*pi*fi);
			float sy = sin(2.f*pi*fj);
			float cy = cos(2.f*pi*fj);
			SETARRAY3(dv->norm,(sy)*cx,(sy)*sx,cy)
			SETARRAY3(dv->pos, (rx+sy*ry)*cx, (rx+sy*ry)*sx, ry*cy);
//			printf("vertex[i]=%.3f %.3f %.3f\n",dvi,dv->pos0],dv->pos[1],dv->pos[2]);
			SETARRAY2(dv->tex0, fi*8.f,fj*2.f);
			auto outf= (sy*0.125f+(1.f-0.125f));
//			dv->color=0xffffffff;
			dv->color[0]=outf*((sin(fi*pi2*6.f)+sin(fj*pi2*1.f))*0.5f*0.05f+0.5f);
			dv->color[1]=outf*((sin(fi*pi2*5.f)+sin(fj*pi2*2.f))*0.5f*0.05f+0.5f);
			dv->color[2]=outf*((sin(fi*pi2*4.f)+sin(fj*pi2*3.f))*0.5f*0.05f+0.5f);
			dv->color[3]=((i&7)<4)?1.f:0.f;
		}
	}
}
#define SH_LOG printf
template<typename T>
auto CreateBuffer(const T* data,size_t num_elems,  int bufferType)->int {
	GLuint	id;
	size_t size = num_elems*sizeof(T);
	glGenBuffers(1,&id);
    SH_LOG("buffer created %d size=%d\n",id, (int)size);
	glBindBuffer(bufferType, id);
	
	glBufferData(bufferType, size, data, GL_STATIC_DRAW);
    auto err=glGetError();
    if (err!=GL_NO_ERROR) {
        if (err==GL_OUT_OF_MEMORY){
            SH_LOG("out of memory attempting to alloc %lu bytes\n",size);
        }
        SH_LOG("gl error=%d",err);
    }

	glBindBuffer(bufferType, 0);
	return	id;
}

GridMesh::GridMesh(int	numU, int numV)
{
	auto m=this;
	m->numVertices=numU*numV;
	m->numIndices = (numV-1)*(2*numU+2);
	vector<TestVertex>	vertices; vertices.resize(numU*numV);
	m->vertexSize = sizeof(Vertex);
	int	i,j;
	FillTorusVertices(&vertices[0], numU,numV);

	vector<IndexType>	indices; indices.resize(m->numIndices);
	// per strip..
	int	dvi=0,vi=0;
	for (j=0; j<(numV-1); j++) 
	{
		// per quad
		indices[dvi++]= j*numU+0;	// start with degenerate
		for (i=0; i<numU; i++) 
		{
			indices[dvi++] = j*numU + i;
			indices[dvi++] = (j+1)*numU + i;
		}
		indices[dvi]= indices[dvi-1];	// end with degenerate
	}
	printf("index debug%d %d %d %d\n",m->IndexSize, m->numIndices, dvi, (int)sizeof(IndexType)*dvi);
    glGenVertexArrays(1, &m->vao);
    glBindVertexArray(m->vao);
	m->ibo = CreateBuffer(&indices[0],m->numIndices, GL_ELEMENT_ARRAY_BUFFER);
	m->vbo = CreateBuffer(&vertices[0],m->numVertices, GL_ARRAY_BUFFER);
	// TODO - supposed to be able to cache all that here.
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,m->ibo);
	glBindBuffer(GL_ARRAY_BUFFER,m->vbo);

	int	stride=sizeof(TestVertex);
	TestVertex* baseVertex=(TestVertex*)0;
	glEnableVertexAttribArray(VertexAttrIndex_pos);
	glEnableVertexAttribArray(VertexAttrIndex_color);
	glEnableVertexAttribArray(VertexAttrIndex_tex0);
	glEnableVertexAttribArray(VertexAttrIndex_norm);

	glVertexAttribPointer(VertexAttrIndex_pos,	3,GL_FLOAT, GL_FALSE,	stride, (void*) &baseVertex->pos);
	glVertexAttribPointer(VertexAttrIndex_color,	4,GL_FLOAT, GL_FALSE,	stride, (void*) &baseVertex->color);
	glVertexAttribPointer(VertexAttrIndex_tex0,	2, GL_FLOAT, GL_FALSE,	stride, (void*) &baseVertex->tex0);
	glVertexAttribPointer(VertexAttrIndex_norm,	3, GL_FLOAT, GL_FALSE,	stride, (void*) &baseVertex->norm);
    glBindVertexArray(0);

}

void	TestGl_Idle();

float	angle=0.f;
GridMesh*	g_pGridMesh;


inline void MatrixStoreIdentity4x4Demo(float m[16]){
    int i; for (i=0; i<16; i++) m[i]=0.f;
    for (i=0; i<4; i++) { m[i+i*4]=1.f;}
}
inline  void MatrixMul4x4Demo(float  result[16],const  float  a[16],const  float  b[16]) {
    int  i,j,k;
    for (i=0; i<4; i++) {
        for (j=0; j<4; j++) {
            float sum=0.f;
            for (k=0; k<4; k++) {
                sum+=a[j*4+k]*b[k*4+i];
            }
            result[j*4+i]=sum;
        }
    }
}


const int MaxLights=4;
enum {
	SU_MatProj = 0,
	SU_MatModelView = 4,
	SU_Ambient = 8,
	SU_DiffuseDX = 9,
	SU_DiffuseDY = 10,
	SU_DiffuseDZ = 11,
	SU_FogColor = 12,
	SU_FogFalloff = 13,
	SU_SpecularDir = 14,
	SU_SpecularPower = 15,
	SU_SpecularColor = 16,
	SU_Tex0 = 17,
	SU_Tex1 = 18,
	SU_Tex2 = 19,
	SU_Tex3 = 20,
	SU_NumLights = 21,
	SU_LightPos = 22,
	SU_LightColor = SU_LightPos+MaxLights,
};
struct Vector4f {float x,y,z,w; Vector4f(float a,float b,float c,float d):x(a),y(b),z(c),w(d){};};
void SetUniform(int index, const Vector4f& v) {
	glUniform4f(index,v.x,v.y,v.z,v.w);
}
void SetUniformLight(int index, const Vector4f& posRadius, const Vector4f& color){
	SetUniform(SU_LightPos+index, posRadius);
	SetUniform(SU_LightColor+index, color);
}
Vector4f g_FogColor={0.25f,0.5f,0.5f,1.f};
void	RenderMeshTextureOveride(Mesh* msh,int t0,int t1) 
{	
	int	i;
	glUniform1i(SU_Tex0, 0);
	glUniform1i(SU_Tex1, 1);
	// specular highlight, only one supported eg representing main sun light?
	SetUniform(SU_SpecularDir, Vector4f{0.0,0.707f,0.707f,0.f});
	SetUniform(SU_SpecularColor, Vector4f{0.5f,0.5f,0.5f,0.f});
	// describe a spherical harmonic - essentially a 4x4 matrix transforming normal vector into a color
	// aproximatly it's an ambient color and variation of color along x,y,z axes.
	// you can approximate environment lights this way.
	SetUniform(SU_Ambient, Vector4f{0.25f,0.25f,0.25f,1.f});
	SetUniform(SU_DiffuseDX, Vector4f(0.0,0.0,0.0,0.0));//Vector4f{0.0f,0.0f,0.25f,1.f});
	SetUniform(SU_DiffuseDY, Vector4f(0.0,0.25,0.0,0.0));// Vector4f{0.25f,0.25f,0.25f,1.f});
	SetUniform(SU_DiffuseDZ, Vector4f(0.0,0.0,0.0,0.0));// Vector4f{0.25f,0.0f,0.0f,1.f});
	// simple depth based fog
	SetUniform(SU_FogColor, g_FogColor);
	SetUniform(SU_FogFalloff, Vector4f{0.5f,0.25f,0.0f,0.f});

	// hardcoded 4 lights
	float lx=0.5;
	float ly=0.5;
	float lz=-0.5;
	float lr=0.7f;
	
	glUniform1i(SU_NumLights,4);
	SetUniformLight(0, Vector4f(lx,ly,lz, lr), Vector4f(1.0, 0.0, 0.0, 0.0));
	SetUniformLight(1, Vector4f(-lx,ly,lz, lr), Vector4f(0.0, 1.0, 0.0, 0.0));
	SetUniformLight(2, Vector4f(lx,-ly,lz, lr), Vector4f(0.0, 0.0, 1.0, 0.0));
	SetUniformLight(3, Vector4f(-lx,-ly,lz, lr), Vector4f(1.0, 0.0, 1.0, 0.0));
		

	glActiveTexture(GL_TEXTURE0+0);	glBindTexture(GL_TEXTURE_2D, t0);
	glActiveTexture(GL_TEXTURE0+1);	glBindTexture(GL_TEXTURE_2D, t1);

	// describe the vertex layout in the vertex buffer..
	glBindVertexArray(msh->vao);
	glDrawElements(GL_TRIANGLE_STRIP, msh->numIndices, GL_UNSIGNED_INT,0);
}



float g_angle=0.f;
int	g_frame=0;

void
MatrixRotate(float m[16], int axis, float angle) {
	MatrixStoreIdentity4x4Demo(m);
	static int axis0[3]={1,0,0};
	static int axis1[3]={2,2,1};
	int	i0=axis0[axis];
	int	i1=axis1[axis];
	float s= sin(angle),c=cos(angle);
	m[i0*4+i0]=c;
	m[i0*4+i1]=s;
	m[i1*4+i0]=-s;
	m[i1*4+i1]=c;
}
void
MatrixTranslate(float m[16], float x, float y, float z) {
	MatrixStoreIdentity4x4Demo(m);
	m[12]=x;
	m[13]=y;
	m[14]=z;
}
int g_NumTorus = 1024;
void	ShaderTest_Render() 
{

	g_angle+=0.0025f;
	glClearColor(g_FogColor.x,g_FogColor.y,g_FogColor.z,g_FogColor.w);
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	float matI[16],matMVP[16], matMV[16], matP[16], matRotX[16],matRotY[16],matTrans[16], matRotXY[16];
	MatrixStoreIdentity4x4Demo(matI);
	SetMatrixProjectionFrustumDemo(matP,-0.5f,0.5f,-0.5f,0.5f,0.9f,10.f);
	int	i;
	float r0 = 0.5f;
	float r1 = 0.1f;
	float sda=0.2f;
	float a0=g_angle*1.1f+0.1f, a1=g_angle*1.09f+1.5f;
	float a2=g_angle*1.05f+0.5f, a3=g_angle*1.11f;
	float a4=g_angle*1.11f+0.7f, a5=g_angle*1.105f;
	float da0=2*PI*0.071f*sda;
	float da1=2*PI*0.042f*sda;
	float da2=2*PI*0.081f*sda;
	float da3=2*PI*0.091f*sda;
	float da4=2*PI*0.153f*sda;
	float da5=2*PI*0.1621f*sda;

	for (i=0; i<g_NumTorus; i++,a0+=da0,a1+=da1,a2+=da2,a3+=da3,a4+=da4,a5+=da5) 
	{
		MatrixTranslate(matTrans, cos(a0)*r0+cos(a3)*r1 , cos(a1)*r0+cos(a4)*r1, cos(a2)*r0+cos(a5)*r1 -2.f*r0);
		MatrixRotate(matRotX, 0, a0);
		MatrixRotate(matRotY, 1, a1*0.245f);
		MatrixMul4x4Demo(matRotXY, matRotX, matRotY);
		MatrixMul4x4Demo(matMV, matRotXY, matTrans);
		MatrixMul4x4Demo(matMVP, matP,matMV);
	

		glUseProgram(g_ShaderProgram);
		glUniformMatrix4fvARB(SU_MatProj, 1,  GL_FALSE, matP);
		glUniformMatrix4fvARB(SU_MatModelView, 1, GL_FALSE, matMV);

		RenderMeshTextureOveride(g_pGridMesh,g_Texture[1+(i&3)],g_Texture[1+((1+i)&3)]);
	}
	glFlush();
	g_frame++;
}

void	CreateTextures() {
//	static_assert(sizeof(GLuint)==sizeof(int));
	glGenTextures(1,(GLuint*)&g_Texture[0]);
	glBindTexture(GL_TEXTURE_2D,g_Texture[0]);
	glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );

	int	usize=256,vsize=256;
	void* buffer = malloc(usize*vsize*4);
	int	i,j;
	int* dst=(int*) buffer;
	for (j=0; j<vsize; j++) {
		for (i=0; i<usize;i++) {	
			dst[i+j*usize] = i+j*256+255*256*256;
		}
	}
	int	m;
	for (m=0; m<8; m++) {
		glTexImage2D(GL_TEXTURE_2D, m, GL_RGB, usize,vsize, 0, GL_RGB, GL_UNSIGNED_BYTE,buffer);
	}
	free(buffer);
	glBindTexture(GL_TEXTURE_2D,0);
	g_Texture[1]=g_Texture[0];
	g_Texture[2]=g_Texture[0];
	g_Texture[3]=g_Texture[0];
	g_Texture[4]=g_Texture[0];
	printf("%s:%d uncomment texture loading here\n");
/*
	g_Texture[1] = getTexture("data/rocktile.tga");
	g_Texture[4] = getTexture("data/pebbles_texture.tga");
	g_Texture[3] = getTexture("data/grass.tga");
	g_Texture[2] = getTexture("data/cliffs.tga");
	*/
}
int main(int argc, const char** argv)
{

	printf("hello SDL world\n");

	SDL_Init(SDL_INIT_EVERYTHING);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);

    SDL_GL_SetSwapInterval(0);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);


    auto wnd(
        SDL_CreateWindow("test", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
            1024, 1024, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN));
    auto glc = SDL_GL_CreateContext(wnd);
	SDL_ShowWindow(wnd);

	CreateShaders();
	CreateTextures();

	glDrawBuffer(GL_BACK);
	glEnable(GL_DEPTH_TEST);
	g_pGridMesh = new GridMesh(16,16);

	int frame=0;
	while (1){
        SDL_Event e;
        while(SDL_PollEvent(&e))
        {
            if(e.type == SDL_QUIT) std::terminate();
        }

		// draw the torus lisajous
		ShaderTest_Render();

		frame++;
		
        SDL_GL_SwapWindow(wnd);
	}
	
}

